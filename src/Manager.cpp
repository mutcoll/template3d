/**
 * @file Manager.cpp
 * @author jmmut
 * @date 2018-05-27.
 */

#include "Manager.h"
void Manager::onDisplay(Uint32 ms) {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glPushMatrix();

    this->FlyerManagerBase::onDisplay( ms);

    drawAxes();
    shaders.setUniform("t", static_cast<int>(time));

    glPopMatrix();
    std::static_pointer_cast< WindowGL>( this->window.lock())->swapWindow();
    ++time;
}

void Manager::drawAxes() const {
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(100, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 100, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 100);
    glEnd();
}

