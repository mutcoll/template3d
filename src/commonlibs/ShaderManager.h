/**
 * @file ShaderManager.h
 * @date 2021-07-10
 */

#ifndef MYTEMPLATEPROJECT_SHADERMANAGER_H
#define MYTEMPLATEPROJECT_SHADERMANAGER_H


#include <fstream>

#include "SDL.h"
#include "SDL_opengl.h"

typedef struct {
    GLhandleARB program;
    GLhandleARB vertShader;
    GLhandleARB fragShader;
    const char *vertSource;
    const char *fragSource;
} ShaderData;

/**
 * Example minimal usage:
 *
 * to enable shaders:
```
shaders.loadShaders("shaders/test.vs.glsl", "shaders/test.fs.glsl");
```
 * (optional) to update uniforms (variables from the CPU sent to the shaders):
```
shaders.setUniform("t", time++);
```
 */
class ShaderManager {
	public:
		ShaderManager(bool requireShaders = true) {
		    if (not initShaders() and requireShaders) {
		        throw std::runtime_error{"Shaders were required but they are not available."};
		    }
		}
		~ShaderManager() { quitShaders(); }


		//////////////// shaders easy API
		void loadShaders(const char vertex_shader_path[],
		                 const char fragment_shader_path[]);

		// only needed if you have loaded several shaders
		[[nodiscard]] int loadShadersAndGetTheirId(const char vertex_shader_path[],
		                                           const char fragment_shader_path[]);
		bool selectShader(int shader_id); // only needed if you have loaded several shaders
		void unselectShader(); // only needed if you have loaded several shaders

		/** Uniforms are a type of variable the program can send to the shaders
		 * @param variable_name as it is used in the shader.
		 * @param variable_value that the program wants to send to the shader.
		 */
		void setUniform(const char variable_name[], float variable_value);
		void setUniform(const char variable_name[], int variable_value);

		//////////////// shaders helpers
		/* you shouldn't need to use this unless doing something I didn't foresee */
		bool initShaders();
		[[nodiscard]] bool areShadersSupported() { return this->shadersSupported; }
		void quitShaders();
		static bool loadFile(const char path[], char* &program, int &length);
		bool compileShader(GLhandleARB, const char*);
		bool compileShaderProgram(ShaderData*);
		void destroyShaderProgram(ShaderData*);

		//////////////// shaders handlers
		/*
		 * handle the shaders at low level. this methods are called from
		 * shaders the above higher-level functions, but if you don't like
		 * them you can call these directly.
		 */
		PFNGLATTACHOBJECTARBPROC glAttachObjectARB = 0;
		PFNGLCOMPILESHADERARBPROC glCompileShaderARB = 0;
		PFNGLCREATEPROGRAMOBJECTARBPROC glCreateProgramObjectARB = 0;
		PFNGLCREATESHADEROBJECTARBPROC glCreateShaderObjectARB = 0;
		PFNGLDELETEOBJECTARBPROC glDeleteObjectARB = 0;
		PFNGLGETINFOLOGARBPROC glGetInfoLogARB = 0;
		PFNGLGETOBJECTPARAMETERIVARBPROC glGetObjectParameterivARB = 0;
		PFNGLLINKPROGRAMARBPROC glLinkProgramARB = 0;
		PFNGLSHADERSOURCEARBPROC glShaderSourceARB = 0;
		PFNGLUSEPROGRAMOBJECTARBPROC glUseProgramObjectARB = 0;


		/////////////// shader variables
		/**
		 * to change the value of a shader variable, you must first get the
		 * location, and later set it.
		 * Shaders have three types of variables: uniforms, attributes and varyings;
		 * but you can only set uniforms and attributes from outside.
		 */
		PFNGLGETUNIFORMLOCATIONARBPROC glGetUniformLocationARB = 0;	// get uniform location

		PFNGLUNIFORM1IARBPROC glUniform1iARB = 0;	//set uniforms
		PFNGLUNIFORM1FARBPROC glUniform1fARB = 0;
		PFNGLUNIFORM2FVARBPROC glUniform2fvARB = 0;
		PFNGLUNIFORM3FVARBPROC glUniform3fvARB = 0;

		PFNGLGETATTRIBLOCATIONARBPROC glGetAttribLocationARB = 0;	// get attributes location

		PFNGLVERTEXATTRIB1FARBPROC glVertexAttrib1fARB = 0;	// set attributes
		PFNGLVERTEXATTRIB2FVARBPROC glVertexAttrib2fvARB = 0;
		PFNGLVERTEXATTRIB3FVARBPROC glVertexAttrib3fvARB = 0;
		PFNGLVERTEXATTRIB1DARBPROC glVertexAttrib1dARB = 0;
		PFNGLVERTEXATTRIB2DVARBPROC glVertexAttrib2dvARB = 0;

	protected:
		static const int NUM_SHADERS = 9;
		ShaderData shaders[NUM_SHADERS];
		int numShaders = 0;
		int currentShader = -1;
		bool shadersSupported = false;
};


#endif //MYTEMPLATEPROJECT_SHADERMANAGER_H
