/**
 * @file ShaderManager.cpp
 * @date 2021-07-10
 */

#include "ShaderManager.h"

bool ShaderManager::compileShader(GLhandleARB shader, const char *source)
{
	GLint status;

	glShaderSourceARB(shader, 1, &source, NULL);
	glCompileShaderARB(shader);
	glGetObjectParameterivARB(shader, GL_OBJECT_COMPILE_STATUS_ARB, &status);
	if (status == 0) {
		GLint length;
		char *info;

		glGetObjectParameterivARB(shader, GL_OBJECT_INFO_LOG_LENGTH_ARB, &length);
		info = SDL_stack_alloc(char, length+1);
		glGetInfoLogARB(shader, length, NULL, info);
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to compile shader:\n%s\n%s", source, info);
		SDL_stack_free(info);

		return false;
	} else {
		return true;
	}
}

bool ShaderManager::compileShaderProgram(ShaderData *data)
{
	//const int num_tmus_bound = 4;
	//int i;
	//GLint location;

	glGetError();

	/* Create one program object to rule them all */
	data->program = glCreateProgramObjectARB();

	/* Create the vertex shader */
	data->vertShader = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
	if (!compileShader(data->vertShader, data->vertSource)) {
		return false;
	}

	/* Create the fragment shader */
	data->fragShader = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
	if (!compileShader(data->fragShader, data->fragSource)) {
		return false;
	}

	/* ... and in the darkness bind them */
	glAttachObjectARB(data->program, data->vertShader);
	glAttachObjectARB(data->program, data->fragShader);
	glLinkProgramARB(data->program);

	/* Set up some uniform variables *//*
	glUseProgramObjectARB(data->program);
	for (i = 0; i < num_tmus_bound; ++i) {
		char tex_name[5];
		SDL_snprintf(tex_name, SDL_arraysize(tex_name), "tex%d", i);
		location = glGetUniformLocationARB(data->program, tex_name);
		if (location >= 0) {
			glUniform1iARB(location, i);
		}
	}
	location = glGetUniformLocationARB(data->program, "t");
	if (location >= 0) {
		glUniform1iARB(location, 120);
	}
*/
	glUseProgramObjectARB(0);

	return glGetError() == GL_NO_ERROR;
}

void ShaderManager::destroyShaderProgram(ShaderData *data)
{
	if (shadersSupported) {
		glDeleteObjectARB(data->vertShader);
		glDeleteObjectARB(data->fragShader);
		glDeleteObjectARB(data->program);
	}
}

/**
 * Obtains the ARB functions, and if they are supported, compiles
 * the shaders. Shaders must be loaded before calling this method.
 */
bool ShaderManager::initShaders()
{
	shadersSupported = false;
	if (SDL_GL_ExtensionSupported("GL_ARB_shader_objects") &&
			SDL_GL_ExtensionSupported("GL_ARB_shading_language_100") &&
			SDL_GL_ExtensionSupported("GL_ARB_vertex_shader") &&
			SDL_GL_ExtensionSupported("GL_ARB_fragment_shader")) {
		glAttachObjectARB = (PFNGLATTACHOBJECTARBPROC) SDL_GL_GetProcAddress("glAttachObjectARB");
		glCompileShaderARB = (PFNGLCOMPILESHADERARBPROC) SDL_GL_GetProcAddress("glCompileShaderARB");
		glCreateProgramObjectARB = (PFNGLCREATEPROGRAMOBJECTARBPROC) SDL_GL_GetProcAddress("glCreateProgramObjectARB");
		glCreateShaderObjectARB = (PFNGLCREATESHADEROBJECTARBPROC) SDL_GL_GetProcAddress("glCreateShaderObjectARB");
		glDeleteObjectARB = (PFNGLDELETEOBJECTARBPROC) SDL_GL_GetProcAddress("glDeleteObjectARB");
		glGetInfoLogARB = (PFNGLGETINFOLOGARBPROC) SDL_GL_GetProcAddress("glGetInfoLogARB");
		glGetObjectParameterivARB = (PFNGLGETOBJECTPARAMETERIVARBPROC) SDL_GL_GetProcAddress("glGetObjectParameterivARB");
		glLinkProgramARB = (PFNGLLINKPROGRAMARBPROC) SDL_GL_GetProcAddress("glLinkProgramARB");
		glShaderSourceARB = (PFNGLSHADERSOURCEARBPROC) SDL_GL_GetProcAddress("glShaderSourceARB");
		glUseProgramObjectARB = (PFNGLUSEPROGRAMOBJECTARBPROC) SDL_GL_GetProcAddress("glUseProgramObjectARB");

		glGetUniformLocationARB = (PFNGLGETUNIFORMLOCATIONARBPROC) SDL_GL_GetProcAddress("glGetUniformLocationARB");
		glUniform1iARB = (PFNGLUNIFORM1IARBPROC) SDL_GL_GetProcAddress("glUniform1iARB");
		glUniform1fARB = (PFNGLUNIFORM1FARBPROC) SDL_GL_GetProcAddress("glUniform1fARB");
		glUniform2fvARB = (PFNGLUNIFORM2FVARBPROC) SDL_GL_GetProcAddress("glUniform2fvARB");
		glUniform3fvARB = (PFNGLUNIFORM3FVARBPROC) SDL_GL_GetProcAddress("glUniform3fvARB");

		glGetAttribLocationARB = (PFNGLGETATTRIBLOCATIONARBPROC) SDL_GL_GetProcAddress("glGetAttribLocationARB");
		glVertexAttrib1fARB = (PFNGLVERTEXATTRIB1FARBPROC) SDL_GL_GetProcAddress("glVertexAttrib1fARB");
		glVertexAttrib2fvARB = (PFNGLVERTEXATTRIB2FVARBPROC) SDL_GL_GetProcAddress("glVertexAttrib2fvARB");
		glVertexAttrib3fvARB = (PFNGLVERTEXATTRIB3FVARBPROC) SDL_GL_GetProcAddress("glVertexAttrib3fvARB");
		glVertexAttrib1dARB = (PFNGLVERTEXATTRIB1DARBPROC) SDL_GL_GetProcAddress("glVertexAttrib1dARB ");
		glVertexAttrib2dvARB = (PFNGLVERTEXATTRIB2DVARBPROC)  SDL_GL_GetProcAddress("glVertexAttrib2dvARB ");
		if (glAttachObjectARB &&
				glCompileShaderARB &&
				glCreateProgramObjectARB &&
				glCreateShaderObjectARB &&
				glDeleteObjectARB &&
				glGetInfoLogARB &&
				glGetObjectParameterivARB &&
				glLinkProgramARB &&
				glShaderSourceARB &&
				glUseProgramObjectARB &&

				glGetUniformLocationARB &&
				glUniform1iARB &&
				glUniform1fARB &&
				glUniform2fvARB &&
				glUniform3fvARB &&

				glGetAttribLocationARB &&
				glVertexAttrib1fARB &&
				glVertexAttrib2fvARB &&
				glVertexAttrib3fvARB &&
				true) {

			if ( glVertexAttrib1dARB &&
						glVertexAttrib2dvARB &&
						true) {
				//shaders_supported = true;
			}
			else
				SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "There's no shader support for double precision float");

			shadersSupported = true;
		}
		else
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "There's no shader support");

	}

	if (!shadersSupported) {
		return false;
	}

	/* We're done! */
	return true;
}

bool ShaderManager::loadFile(const char path[], char *&source, int &length)
{
	std::ifstream f;

	f.open(path);
	if (!f)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't open file %s", path);
		return false;
	}

	f.seekg(0, f.end);
	length = f.tellg();
	f.seekg(0, f.beg);

	source = new char[length+1];

	f.read(source, length);
	source[length++] = 0; // end of string

	f.close();

	return true;
}


int ShaderManager::loadShadersAndGetTheirId(const char vertex_shader_path[],
                                            const char fragment_shader_path[]) {
    char* program;
    int length;

    if (numShaders == NUM_SHADERS)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Shaders array is full. Try increasing NUM_SHADERS.");
        return false;
    }

    /* get shader source from files. Stop writing shaders within quotes! */
    shaders[numShaders].vertSource = loadFile(vertex_shader_path, program, length) ? program : NULL;
    shaders[numShaders].fragSource = loadFile(fragment_shader_path, program, length) ? program : NULL;

    /* Compile the shaders */
    if (!compileShaderProgram(&shaders[numShaders])) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to compile shader!\n");
        return false;
    }

    currentShader = numShaders++;

    if (not shaders[currentShader].vertSource or not shaders[currentShader].fragSource) {
        throw std::runtime_error{
                std::string{"could not load shaders. vertex: "} + vertex_shader_path
                        + ", fragment: " + fragment_shader_path};
    }
    selectShader(currentShader);
    return currentShader;
}
void ShaderManager::loadShaders(const char *vertex_shader_path, const char *fragment_shader_path) {
    int id = loadShadersAndGetTheirId(vertex_shader_path, fragment_shader_path);
    (void)id; // silence warning about unused variable
}

void ShaderManager::quitShaders()
{
	int i;

	for (i = 0; i < numShaders; ++i) {
		destroyShaderProgram(&shaders[i]);
	}
}

bool ShaderManager::selectShader(int n)
{
	if (n >= numShaders || n < 0)
	{
		SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "Invalid attemp to select shader %d, unselecting shader...", n);
		glUseProgramObjectARB(0);
		return false;
	}

	currentShader = n;
	glUseProgramObjectARB(shaders[currentShader].program);

	return true;
}

void ShaderManager::unselectShader()
{
	glUseProgramObjectARB(0);
}

/**
 * Function provided as example.
 * maybe a more generic version will be implemented,
 * posibly with a void* and runtime type inferring
 */
void ShaderManager::setUniform(const char variable_name[], float variable_value)
{
    GLint location = glGetUniformLocationARB(shaders[currentShader].program, variable_name);
    if (location >= 0) {
        glUniform1fARB(location, variable_value);
    } else {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                    "setUniform: uniform %s in shader with id %d does not exist", variable_name,
                    currentShader);
    }
}

void ShaderManager::setUniform(const char variable_name[], int variable_value)
{
    GLint location = glGetUniformLocationARB(shaders[currentShader].program, variable_name);
    if (location >= 0) {
        glUniform1iARB(location, variable_value);
    } else {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                    "setUniform: uniform %s in shader with id %d does not exist",
                    variable_name, currentShader);
    }
}


