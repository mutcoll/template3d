
#include "randomize/exception/SignalHandler.h"
#include "Manager.h"

std::shared_ptr<WindowGL> setupWindow(Dispatcher &dispatcher) {
    std::shared_ptr< WindowGL> window = std::make_shared< WindowGL>();
    window->open();
    window->initGL();
    dispatcher.addEventHandler( [&window] (SDL_Event &event){ window->close();},
            [windowID = window->getWindowID()] (SDL_Event &event){
                return event.type == SDL_WINDOWEVENT
                        && event.window.windowID == windowID
                        && event.window.event == SDL_WINDOWEVENT_CLOSE;
            }
    );
    return window;
}

void runDispatcherAndExit() {
    Dispatcher dispatcher;
    dispatcher.addEventHandler( [&dispatcher] (SDL_Event &event){ dispatcher.endDispatcher();},
            [] (SDL_Event &event){
                return event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE);
            }
    );

    std::shared_ptr<WindowGL> window = setupWindow(dispatcher);
    Manager manager(dispatcher, window);
    manager.activate();

    dispatcher.startDispatcher();
    exit(0);
}

int main() {
    randomize::exception::SignalHandler::activate();
    runDispatcherAndExit();
    return 0;
}
