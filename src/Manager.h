/**
 * @file Manager.h
 * @author jmmut
 * @date 2018-05-27.
 */

#ifndef MYTEMPLATEPROJECT_MANAGER_H
#define MYTEMPLATEPROJECT_MANAGER_H


#include "commonlibs/ShaderManager.h"
#include "randomize/exception/StackTracedException.h"
#include "randomize/events/Dispatcher.h"
#include "randomize/events/EventCallThis.h"
#include "randomize/manager_templates/FlyerManagerBase.h"

class Manager : public FlyerManagerBase {
public:
    Manager( Dispatcher &d, std::shared_ptr< WindowGL> w): FlyerManagerBase( d, w) {
        shaders.loadShaders("shaders/test.vs.glsl", "shaders/test.fs.glsl");
    }
protected:
    void onDisplay( Uint32 ms) override;

    void drawAxes() const;

private:
    ShaderManager shaders;
    long time = 0;
};


#endif //MYTEMPLATEPROJECT_MANAGER_H
