# Template project

To get an empty project that compiles and already draws something, check that you have the requirements 
installed, and do the following:

Linux:

```
git clone https://bitbucket.org/mutcoll/template3d.git
cd template3d
mkdir build && cd build
conan install --build missing ..
cmake -G "Unix Makefiles" ..
make
./bin/mytemplate
```

Windows:

```
git clone https://bitbucket.org/mutcoll/template3d.git
cd template3d
conan install --build missing
cmake -G "Visual Studio 14 Win64"
cmake --build . --config Release
./bin/mytemplate.exe
```

## Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended:
  `sudo apt-get install python-pip; sudo pip install setuptools wheel conan`)
